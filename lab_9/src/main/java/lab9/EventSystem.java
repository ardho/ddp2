package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.math.BigInteger;

/**
* Class representing event managing system
*/
public class EventSystem {
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
    * Accessor for event. 
    * @return Event (object) in events
    */
    public Event getEvent(String eventName) {
        for(Event event: events) {
            if (event.getName().equalsIgnoreCase(eventName)) {
                return event;
            }
        }
        return null;
    }

    /**
    * Accessor for user. 
    * @return User (object) in users
    */
    public User getUser(String userName) {
        for(User user: users) {
            if (user.getName().equalsIgnoreCase(userName))
                return user;
        }
        return null;
    }

    /**
    * Method to add event to events.
    * @return String success if there's no eventName in events, or otherwise
    */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        try {
            Date startTime = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").parse(startTimeStr);
            Date endTime = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").parse(endTimeStr);
            if (startTime.after(endTime)) {
                return String.format("Waktu yang diinputkan tidak valid!");
            }
            if (getEvent(name) != null) {
                return String.format("Event %s sudah ada!", name);
            }
            BigInteger costPerHour = new BigInteger(costPerHourStr);
            events.add(new Event(name, startTime, endTime, costPerHour));
            return String.format("Event %s berhasil ditambahkan!", name);
        }
        catch (Exception e) {
            return String.format("Format tanggal salah.");
        }
    }
    
    /**
    * Method to add user to users.
    * @return String success if there's no userName in users, or otherwise
    */
    public String addUser(String name) {
        if (getUser(name) != null) {
            return String.format("User %s sudah ada!", name);
        }
        users.add(new User(name));
        return String.format("User %s berhasil ditambahkan!", name);
    }
    
    /**
    * Method to register event to List of events user plans to attend.
    * @return String, based on lab 9 instruction
    */
    public String registerToEvent(String userName, String eventName) {
        Event currentEvent = getEvent(eventName);
        User currentUser = getUser(userName);

        if (currentEvent == null && currentUser == null) {
            return String.format("Tidak ada pengguna dengan nama %s dan acara dengan nama %s!",
                                    userName, eventName);
        }
        else if (currentEvent == null) {
            return String.format("Tidak ada acara dengan nama %s!", eventName);
        }
        else if (currentUser == null) {
            return String.format("Tidak ada pengguna dengan nama %s!", userName);
        }
        else {
            if (!currentUser.addEvent(currentEvent)) {
                return String.format("%s sibuk sehingga tidak dapat menghadiri %s!",
                    userName, eventName);
            }
        }
        return String.format("%s berencana menghadiri %s!", userName, eventName);
    }
}
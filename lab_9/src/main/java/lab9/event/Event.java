package lab9.event;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.math.BigInteger;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event> {
    /**
    * Instance variable of event:
    * name, cost/hour, start & end time.
    */
    private String name;
    private BigInteger costPerHour;
    private Date startTime, endTime;

    /**
    * Constructor. Initializes Event with name, start - end time, and cost/hour.
    */
    public Event(String name, Date startTime, Date endTime, BigInteger costPerHour) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.costPerHour = costPerHour;
    }

    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName() {
        return this.name;
    }

    /**
    * Accessor for start date. 
    * @return start time (Date) of this event instance
    */
    public Date getStartTime() {
        return this.startTime;
    }

    /**
    * Accessor for end date. 
    * @return end time (Date) of this event instance
    */
    public Date getEndTime() {
        return this.endTime;
    }
    
    /**
    * Accessor for cost/hour. 
    * @return cost (BigInteger) of this event instance
    */
    public BigInteger getCost() {
        return this.costPerHour;
    }

    /**
    * Method to print description of this event.
    *
    * @return String, based on lab 9 instruction
    */
    public String toString() {
        return  String.format("%s\n", this.getName()) +
                String.format("Waktu mulai: %1$te-%1$tm-%1$tY, %1$tH:%1$tM:%1$tS%n", 
                    this.getStartTime()) +
                String.format("Waktu selesai: %1$te-%1$tm-%1$tY, %1$tH:%1$tM:%1$tS%n", 
                    this.getEndTime()) +
                String.format("Biaya kehadiran: %d", this.getCost());
    }
    
    /**
    * Method to check is overlaps with other event.
    *
    * @return boolean whether isOverlap or not
    */
    public boolean isOverlapsWith(Event event) {
        if (this.getStartTime().getTime() < event.getEndTime().getTime() &&
            event.getStartTime().getTime() < this.getEndTime().getTime()) {
            return true;
        }
        return false;
    }

    /** 
    * Compares two Dates for ordering.
    *
    * @return the value 0 if the argument Date is equal to this Date; 
    *         a value less than 0 if this Date is before the Date argument; 
    *         and a value greater than 0 if this Date is after the Date argument. 
    */
    public int compareTo(Event other) {
        return startTime.compareTo(other.getStartTime());
    }
}
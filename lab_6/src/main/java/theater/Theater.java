package theater;
import java.util.ArrayList;
import movie.Movie;
import ticket.Ticket;

public class Theater{
	private String nama;
	private int saldo, jumlah;
	private Movie[] film;
	private ArrayList<Ticket> listTiket;

	// nama, saldo bioskop, list tiket, array film
	public Theater(String nama, int saldo, ArrayList<Ticket> listTiket, Movie[] film){
		this.nama = nama;
		this.saldo = saldo;
		this.film = film;
		this.listTiket = listTiket;
	}

	public String getNama(){
		return this.nama;
	}

	public void setSaldo(int jumlah){
		this.saldo = jumlah;
	}

	public int getSaldo(){
		return this.saldo;
	}

	public void setListTiket(Ticket tiket){
		this.listTiket.add(tiket);
	}

	public ArrayList<Ticket> getListTiket(){
		return this.listTiket;
	}

	public int getNTiket(){
		return this.listTiket.size();
	}

	public Movie[] getListFilm(){
		return this.film;
	}

	public int getNFilm(){
		return this.getListFilm().length;
	}

	public void printInfo(){
		String daftar_film = "";
		for (int idx = 0; idx < this.getNFilm(); idx++){
			daftar_film += this.getListFilm()[idx].getJudul();
			
			if (idx+1 != this.getNFilm()){
				daftar_film += ", ";
			}
		}

		System.out.println("------------------------------------------------------------------"); 
		System.out.printf("Bioskop                 : %s\n", this.getNama());
		System.out.printf("Saldo Kas               : %d\n", this.getSaldo());
		System.out.printf("Jumlah tiket tersedia   : %d\n", this.getNTiket());
		System.out.printf("Daftar Film tersedia    : %s\n", daftar_film);
		System.out.println("------------------------------------------------------------------");
	}

	public static int getTotalSaldo(Theater[] bioskop){
		int total = 0;
		for (int idx = 0; idx < bioskop.length; idx++)
			total += bioskop[idx].getSaldo();
		return total;
	}

	public static void printTotalRevenueEarned(Theater[] bioskop){
		System.out.printf("Total uang yang dimiliki Koh Mas : Rp. %d\n", getTotalSaldo(bioskop));
		System.out.println("------------------------------------------------------------------");
		for (int idx = 0; idx < bioskop.length; idx++){
			System.out.printf("Bioskop         : %s\n", bioskop[idx].getNama());
			System.out.printf("Saldo Kas       : Rp. %d\n\n", bioskop[idx].getSaldo());
		}
		System.out.println("------------------------------------------------------------------");
	}
}
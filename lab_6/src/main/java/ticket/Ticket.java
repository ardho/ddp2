package ticket;

import movie.Movie;

public class Ticket{
	private Movie film;
	private int harga;
	private boolean jenis; // True = 3 Dimensi
	private String jadwal;

	// film, jadwal(hari), jenis(3 Dimensi/Biasa)
	public Ticket(Movie film, String jadwal, boolean jenis){
		this.film = film;
		this.jadwal = jadwal; 
		this.jenis = jenis;
		this.harga = 60000;
		if (jadwal.equals("Sabtu") || jadwal.equals("Minggu"))
			this.harga += 40000;
		if (jenis)
			this.harga += 0.2*this.harga; 
	}

	public Movie getFilm(){
		return this.film;
	}

	public int getHarga(){
		return this.harga;
	}

	public boolean getJenis(){
		return this.jenis;
	}

	public String getJadwal(){
		return this.jadwal;
	}
}
import java.util.Scanner;

public class RabbitHouse{
	// f(n) = (n*f(n-1)) + 1, f(1) = 1
	private static int normal(int x){ 
	if(x==1)
		return 1;
	return x * (normal(x-1)) + 1;
	}

	// cek apakah s palindrom
	private static Boolean isPal(String s){ 
		if(s.length() <= 1)
			return true;
		if(s.charAt(0) != s.charAt(s.length() - 1))
			return false;
		return isPal(s.substring(1, s.length() - 1));
	}

	// hapus karakter ke idx pada s
	private static String delCharAt(String s, int idx){ 
		String res = "";
		for(int i = 0; i < s.length(); i++)
			if(i != idx)
				res += s.charAt(i);
		return res;
	}

	// soal bonus
	private static int bonus(String s){
		int ans = 0;
		if(isPal(s))
			return 0;
		if(isPal(s) == false)
			ans++;
		// hitung bonus(s) lagi (rekursif) dengan s tanpa index ke i
		for(int i = 0; i < s.length(); i++){ 
			if(isPal(delCharAt(s, i)) == false){
				ans += bonus(delCharAt(s, i));
			}
		}
		return ans;
	}

	public static void main(String[] args){		
		// input
		Scanner input = new Scanner(System.in); 
		String tipe = input.next();
		String str = input.next();

		if(tipe.equals("palindrom"))
			System.out.println(bonus(str));
		else
			System.out.print(normal(str.length()));
	}
}
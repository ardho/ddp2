import java.util.Scanner;

/**
* @author Yusuf T. Ardho | 1706074985 | DDP2-D | GitLab Acc: rdo-
**/

public class SistemSensus {
	public static void main(String[] args) {
		// New input scanner
		Scanner input = new Scanner(System.in);

		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n--------------------\n");
		System.out.print("Nama Kepala Keluarga   : "); String nama = input.nextLine();
		System.out.print("Alamat Rumah           : "); String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : "); short panjang = Short.parseShort(input.next());
			if(panjang <= 0 || panjang > 250){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
		System.out.print("Lebar Tubuh (cm)       : "); short lebar = Short.parseShort(input.next());
			if(lebar <= 0 || lebar > 250){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
		System.out.print("Tinggi Tubuh (cm)      : "); short tinggi = Short.parseShort(input.next());
			if(tinggi <= 0 || tinggi > 250){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
		System.out.print("Berat Tubuh (kg)       : "); double berat = Double.parseDouble(input.next());
			if(berat <= 0 || berat > 150){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
		System.out.print("Jumlah Anggota Keluarga: "); byte makanan = Byte.parseByte(input.next()); input.nextLine();
			if(makanan <= 0 || makanan > 250){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
		System.out.print("Tanggal Lahir          : "); String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : "); String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : "); byte jumlahCetakan = Byte.parseByte(input.next()); input.nextLine(); System.out.print("\n");

		// Hitung rasio berat per volume (rumus lihat soal)
		double rasio = berat / ((double)panjang * 0.01 * (double)lebar * 0.01 * (double)tinggi * 0.01);

		// Periksa ada catatan atau tidak
		if (catatan == "") catatan = "Tidak ada catatan tambahan";

		for(int i=1; i<=jumlahCetakan; i++){
			// Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase
			
			System.out.print("DATA SIAP DICETAK UNTUK " + penerima + "\n");
			System.out.print("--------------------\n");

			// Cetak hasil (ganti string kosong agar keluaran sesuai)
			System.out.print(nama + " - " + alamat + "\n");
			System.out.print("Lahir pada tanggal " + tanggalLahir + "\n");
			System.out.print("Rasio Berat Per volume = " + (int)rasio + " kg/m^3" + "\n");
			if(catatan.length() == 0) System.out.print("Tidak ada catatan tambahan\n");
			else System.out.print("Catatan: " + catatan + "\n");
			System.out.print("\n");
		}

		// Soal bonus "Rekomendasi Apartemen"
		// Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		int kalkulasi = (panjang * lebar * tinggi);
		for(int i=0; i<nama.length(); i++)
			kalkulasi += nama.charAt(i);
		kalkulasi %= 10000;

		// Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.charAt(0) + String.valueOf(kalkulasi % 10000);;

		// Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = 50000 * 365 * (int)makanan;

		// Hitung umur dari tanggalLahir (rumus lihat soal)
		int tahunLahir = Integer.parseInt(tanggalLahir.split("-")[2]); // lihat hint jika bingung
		int umur = 2018 - tahunLahir;

		// Lakukan proses menentukan apartemen (kriteria lihat soal)
		System.out.print("\nREKOMENDASI APARTEMEN\n--------------------\n");
		System.out.print("MENGETAHUI: Identitas keluarga: " + nama  + " - " + nomorKeluarga + "\n");
		System.out.print("MENIMBANG : Anggaran makanan tahunan: Rp " + anggaran + "\n");
		System.out.print("\t    Umur kepala keluarga: " + umur + " tahun\n");

		// Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String nama_rek, kabupaten;
		if(umur >= 0 && umur <= 18){
			nama_rek = "PPMT"; kabupaten = "Rotunda";
		}
		else if((umur >= 19 && umur <= 1018) && (anggaran <= 100000000)){
			nama_rek = "Teksas"; kabupaten = "Sastra";
		}
		else{
			nama_rek = "Mares"; kabupaten = "Margonda";
		}
		System.out.print("MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n");
		System.out.print(nama_rek + ", kabupaten " + kabupaten);

		input.close();
	}
}
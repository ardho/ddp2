package karyawan;
import java.util.*;

public abstract class Karyawan {
    protected String nama, tipe;
  	protected int gaji;
    protected int total_gajian;
    protected ArrayList<Karyawan> bawahan;

    public void setTotalGajian(int x){
    	this.total_gajian = x;
    	if (this.total_gajian % 6 == 0){
    		System.out.printf("%s mengalami kenaikan gaji sebesar 10 dari %d menjadi %d\n", 
    			this.getNama(), this.getGaji(), this.getGaji()+this.getGaji()/10);
    		this.setGaji(this.getGaji() + this.getGaji()/10 );
    	}
    }

    public void setBawahan(ArrayList<Karyawan> listBawahan){
    	this.bawahan = listBawahan;
    }

    public void setTipe(String tipe){
    	this.tipe = tipe;
    }

   	public void setGaji(int gaji){
   		this.gaji = gaji;
   	}

   	public ArrayList<Karyawan> getBawahan(){
   		return this.bawahan;
   	}

   	public int getTotalGajian(){
   		return this.total_gajian;
   	}

   	public String getTipe(){
   		return this.tipe;
   	}

   	public String getNama(){
   		return this.nama;
   	}

   	public int getGaji(){
   		return this.gaji;
   	}

   	public abstract void status();
}
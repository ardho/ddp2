package karyawan;
import java.util.*;

public class Pekerja extends Karyawan {
	public Pekerja(String nama, int gaji, String tipe){
		this.nama = nama;
		this.gaji = gaji;
		this.tipe = tipe;
		this.bawahan = new ArrayList<Karyawan>();
		this.total_gajian = 0;
	}

	@Override
	public void status(){
		System.out.printf("%s %d\n", nama, gaji);
	}
}
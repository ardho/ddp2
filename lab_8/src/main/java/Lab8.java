import karyawan.*;
import java.util.*;

class Lab8 {

	static Scanner scan = new Scanner(System.in);
	static ArrayList<Karyawan> listKaryawan = new ArrayList<Karyawan>();
	static ArrayList<Karyawan> listCalonPromosi = new ArrayList<Karyawan>();
	static String cmd, nama, nama2, tipe, atasan, bawahan;
    static int gaji, staff_maksimal;
    static Karyawan tmp, tmp2;

    public static void main(String[] args) {
    	staff_maksimal = scan.nextInt();

    	while (true) {
    		cmd = scan.next();

    		if (cmd.equals("TAMBAH_KARYAWAN")) 
    			tambahKaryawan();
			else if (cmd.equals("STATUS"))
				statusKaryawan();
			else if (cmd.equals("TAMBAH_BAWAHAN"))
				tambahBawahan();
			else if (cmd.equals("GAJIAN"))
				gajianKaryawan();
			else{
				System.out.printf("== program selesai ==\n");
				break;
			}
    	}
    }

    public static void tambahKaryawan(){
    	nama = scan.next(); 
    	tipe = scan.next();
    	gaji = scan.nextInt();

    	if (find(nama) != null)
    		System.out.printf("Karyawan dengan nama %s telah terdaftar\n", nama);

    	else {
	    	if (tipe.equals("MANAGER")) 
	    		listKaryawan.add(new Manager(nama, gaji, "MANAGER"));
	    	else if (tipe.equals("STAFF")) 
	    		listKaryawan.add(new Staff(nama, gaji, "STAFF"));
	    	else 
	    		listKaryawan.add(new Intern(nama, gaji, "INTERN"));

	    	System.out.printf("%s mulai bekerja sebagai %s di PT. TAMPAN\n", nama, tipe);
    	}
    }

    public static void statusKaryawan(){
    	nama = scan.next();
    	tmp = find(nama);
    	if (tmp == null)
    		System.out.printf("Karyawan tidak ditemukan\n");
    	else
    		tmp.status();
    }

    public static void tambahBawahan(){
    	atasan = scan.next();
    	bawahan = scan.next();

    	if (!((find(atasan) != null) && (find(bawahan) != null)))
    		System.out.println("Nama tidak berhasil ditemukan");
    	else {
    		tmp = find(atasan);
    		tmp2 = find(bawahan);

    		if ((tmp.getTipe().equals("MANAGER") && tmp2.getTipe().equals("MANAGER")) ||
    			(tmp.getTipe().equals("STAFF") && !tmp2.getTipe().equals("INTERN")) ||
    			(tmp.getTipe().equals("INTERN")))
    			System.out.println("Anda tidak layak memiliki bawahan");
    		else {
    			if (findBawahan(tmp, tmp2))
    				System.out.printf("Karyawan %s telah menjadi bawahan %s\n", bawahan, atasan);
    			else {
    				tmp.getBawahan().add(tmp2);
    				System.out.printf("Karyawan %s berhasil ditambahkan menjadi bawahan %s\n",
    					bawahan, atasan);
    			}
    		}
    	}
    }

    public static void gajianKaryawan(){
    	System.out.println("Semua karyawan telah diberikan gaji");
    	int idx = 0, now = 0;
    	while (idx < listKaryawan.size()) {
    		tmp = listKaryawan.get(idx);
    		tmp.setTotalGajian(tmp.getTotalGajian()+1);
    		if (tmp.getTipe().equals("STAFF") && tmp.getGaji() >= staff_maksimal){
    			listCalonPromosi.add(new Manager(tmp.getNama(), tmp.getGaji(), "MANAGER"));
    			listCalonPromosi.get(now).setBawahan(tmp.getBawahan());
    			now++;
    			System.out.printf("Selamat, %s telah dipromosikan menjadi MANAGER\n", tmp.getNama());
    			listKaryawan.remove(idx);
    		}
    		else
    			idx++;
    	}
    	for(Karyawan data: listCalonPromosi)
    		listKaryawan.add(new Manager(data.getNama(), data.getGaji(), "MANAGER"));
    	listCalonPromosi.clear();
    }

    public static boolean findBawahan(Karyawan atasan, Karyawan bawahan){
    	for (Karyawan data: atasan.getBawahan())
    		if (data.getNama().equals(bawahan.getNama()))
    			return true;
    	return false;
    }

	public static Karyawan find(String nama){
    	for (Karyawan data: listKaryawan) if (data.getNama().equals(nama))
    		return data;
    	return null;
	}
}
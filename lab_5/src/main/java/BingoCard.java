/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Anda diwajibkan untuk mengimplementasikan method yang masih kosong
 * Anda diperbolehkan untuk menambahkan method jika dibutuhkan
 * HINT : Bagaimana caranya cek apakah sudah menang atau tidak? Mungkin dibutuhkan method yang bisa membantu? Hmmmm.
 * Semangat ya :]
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates;
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates(){
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates){
		this.numberStates = numberStates;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public void adaBingo(){
		int hor, ver, diagR = 0, diagL = 0;
		for(int i = 0; i < 5; i++){
			hor = 0; ver = 0;
			for(int j = 0; j < 5; j++){
				if(numbers[i][j].isChecked()){
					if(i == j)
						diagR++;
					if(i+j == 4)
						diagL++;
					hor++;
				}
				if(numbers[j][i].isChecked())
					ver++;
				if(diagL == 5 || diagR == 5 || hor == 5 || ver == 5){
					this.setBingo(true);
				}
			}
		}
	}

	public String markNum(int num){
		//TODO Implementx
		if(numberStates[num] == null)
			return "Kartu tidak memiliki angka " + num;
		if(numberStates[num].isChecked())
			return num + " sebelumnya sudah tersilang";
		numberStates[num].setChecked(true);
		this.adaBingo();
		return num + " tersilang";
	}	

	public String info(){
		String now = "";
		for(int i = 0; i < 5; i++){
			for(int j = 0; j < 5; j++){
				now += "| ";
				if(numbers[i][j].isChecked())
					now += "X  ";
				else
					now += String.valueOf(numbers[i][j].getValue()) + " ";
				if(j == 4){
					now += "|";
					if(i != 4)
						now += "\n";
				}	
			}
		}
		return now;
	}
	
	public void restart(){
		//TODO Implement
		for(int i = 0; i < 5; i++){
			for(int j = 0; j < 5; j++){
				numbers[i][j].setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}

}


